/*
Challenge #85:

This problem was asked by Facebook.

Given three 32-bit integers x, y, and b, return x if b is 1 and y if b is 0, using only mathematical or bit operations. You can assume b can only be 1 or 0.

*/

function xy(x,y,b) {
	if ((b === 1 || b === 0) && typeof x == "number" && typeof y == "number") {
		return (x-y)*b + y
	} else {
		console.log("Invalid input - x, y must be numbers and b must be either 0 or 1")
	}
}

// Testing

console.log(xy(63737,88383,0))
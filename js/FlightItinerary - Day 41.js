/*
Challenge #41:

This problem was asked by Facebook.

Given an unordered list of flights taken by someone, each represented as (origin, destination) pairs, and a starting airport, compute the person's itinerary. If no such itinerary exists, return null. If there are multiple possible itineraries, return the lexicographically smallest one. All flights must be used in the itinerary.

For example, given the list of flights [('SFO', 'HKO'), ('YYZ', 'SFO'), ('YUL', 'YYZ'), ('HKO', 'ORD')] and starting airport 'YUL', you should return the list ['YUL', 'YYZ', 'SFO', 'HKO', 'ORD'].

Given the list of flights [('SFO', 'COM'), ('COM', 'YYZ')] and starting airport 'COM', you should return null.

Given the list of flights [('A', 'B'), ('A', 'C'), ('B', 'C'), ('C', 'A')] and starting airport 'A', you should return the list ['A', 'B', 'C', 'A', 'C'] even though ['A', 'C', 'A', 'B', 'C'] is also a valid itinerary. However, the first one is lexicographically smaller.

*/

function createItinerary(flights, start) {
	var numFlights = flights.length;
	var nextStop = start;
	var tempNextStop = [start,null]; // var to store the mosylr lexicographically up to date next stop, including its location in the current flights list
	var itinerary = [];
	
	for (i=0; i<numFlights; i++) {
		for (j=0; j<flights.length; j++) {
			// checking if nextStop == tempNextStop[0], because if so, it means that the algorithm has not found a trip matching the nextStop yet
			if (flights[j][0]==nextStop && nextStop==tempNextStop[0]) {
				tempNextStop = [flights[j][1],j]
				itinerary.push(flights[j]);
			// checking if nextStop != tempNextStop[0], because if so, it means there is another trip with the same nextStop and we need to check which one is lexpgraphically smallest
			} else if (flights[j][0]==nextStop && nextStop != tempNextStop[0]) {
				// checking if the current trip is lexpgraphically smaller than the previous trip - if so, pop the previous trip's entry from the itinerary, replace with current one and update tempNextStop
				if (flights[j][1] < tempNextStop[0]) {
					itinerary.pop();
					tempNextStop = [flights[j][1],j];
					itinerary.push(flights[j]);
				}
			}
		}
		nextStop = tempNextStop[0];
		flights.splice(tempNextStop[1],1);
	}
	
	if (itinerary.length = numFlights) {
		return itinerary;
	} else {
		return null
	}
}

// Testing

console.log(createItinerary(
	[['A', 'B'], ['A','C'], ['B', 'C'], ['C', 'A']],
	'A'
	))
/* 
Challenge #43:

This problem was asked by Amazon.

Implement a stack that has the following methods:

 - push(val), which pushes an element onto the stack
 - pop(), which pops off and returns the topmost element of the stack. If there are no elements in the stack, then it should throw an error or return null.
 - max(), which returns the maximum value in the stack currently. If there are no elements in the stack, then it should throw an error or return null.

Each method should run in constant time.

*/

function Stack() {
	this._max = null;
	this._length = 0;
	this._stack = [];
	
	this.push = function(val) {
		this._stack[this._length] = val;
		this._length++;

		if (val > this._max || this._max === null) {
			this._max = val;
		};
		
		return this;
	};
	
	this.pop = function() {
		if (this._length===0) {
			return this;
		} else {
			this._length--
			this._stack = this._stack.slice(0,_length);
			return this;
		}
	};
};

// Testing

var someStack = new Stack;

console.log(someStack.push(4).push(3)._max)
/*
Challenge #42:

This is your coding interview problem for today.

This problem was asked by Google.

Given a list of integers S and a target number k, write a function that returns a subset of S that adds up to k. If such a subset cannot be made, then return null.

Integers can appear more than once in the list. You may assume all numbers in the list are positive.

For example, given S = [12, 1, 61, 5, 9, 2] and k = 24, return [12, 9, 2, 1] since it sums up to 24.

*/

function findSum(numList, sumTo) {
	var total = sumTo;
	var usedNums = [];
	
	// sorting numList in descending order
	numList.sort((a,b) => {return b-a})
	
	for (i=0; i<numList.length; i++){
		if (total<numList[i]) {
			continue
		} else {
			total -= numList[i];
			usedNums.push(numList[i]);
			
			console.log(total, usedNums)
		}
		
		if (total==0) {
			return usedNums;
		}
	}
	
	return "No subset that can sum to target"
}

// Testing

console.log(findSum([24,17,7,3,2,2,1],15))
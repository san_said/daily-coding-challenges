// OUTPUTTING ERRORS

/*
Challenge #84:

This problem was asked by Amazon.

Given a matrix of 1s and 0s, return the number of "islands" in the matrix. A 1 represents land and 0 represents water, so an island is a group of 1s that are neighboring and their perimiter is surrounded by water.

For example, this matrix has 4 islands.

1 0 0 0 0
0 0 1 1 0
0 1 1 0 0
0 0 0 0 0
1 1 0 0 1
1 1 0 0 1

*/

const MOVES = [[0,1],[0,-1],[1,0],[-1,0]]

function turnToZeros(x, y, matrix, mW, mH) {
	matrix[x][y] = 0;
	
	for (i=0; i<MOVES.length; i++) {
		if (0 <= x+MOVES[i][0] < mW && 0 <= y+MOVES[i][1] < mH) {
			if (matrix[y+MOVES[i][1]][x+MOVES[i][0]] == 1) {
				turnToZeros(x+MOVES[i][0],y+MOVES[i][1],matrix, mW, mH)
			}
		}
	}
}


function numberOfIslands(matrix) {
	var count = 0;
	var mW = matrix[0].length;
	var mH = matrix.length;
	
	for (i=0; i<mH; i++) {
		for (j=0; j<mW; j++) {
			if (matrix[i][j] == 1) {
				count++;
				turnToZeros(j, i, matrix, mW, mH)
			}
		}
	}
	
	return count;
}

// Testing

numberOfIslands([[1,0,0],[0,1,1],[0,1,1]])
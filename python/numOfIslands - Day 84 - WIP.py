## INCORRECT ##

"""
Challenge #84:

This problem was asked by Amazon.

Given a matrix of 1s and 0s, return the number of "islands" in the matrix. A 1 represents land and 0 represents water, so an island is a group of 1s that are neighboring and their perimiter is surrounded by water.

For example, this matrix has 4 islands.

1 0 0 0 0
0 0 1 1 0
0 1 1 0 0
0 0 0 0 0
1 1 0 0 1
1 1 0 0 1

"""

MOVES = [[0,1],[0,-1],[1,0],[-1,0]]

def turnToZeros(x, y, matrix, mW, mH):
    print(matrix)
    matrix[x][y] = 0;
    for k in range(len(MOVES)):
        if (0 <= x+MOVES[k][0] < mW) and (0 <= y+MOVES[k][1] < mH):
            if (matrix[y+MOVES[k][1]][x+MOVES[k][0]] == 1):
              turnToZeros(x+MOVES[k][0],y+MOVES[k][1],matrix, mW, mH)


def numberOfIslands(matrix):
    count = 0
    mW = len(matrix[0])
    mH = len(matrix)
    
    for i in range(mH):
        for j in range(mW):
            if (matrix[i][j] == 1):
                count+=1
                turnToZeros(j, i, matrix, mW, mH)

    print(count)

## Testing

numberOfIslands([[1,0,0],[0,1,1],[0,1,1]])

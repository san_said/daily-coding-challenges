import sys

def validate_brackets(bracks):
  stack = []
  brack_hash = {
    "}": "{",
    ")": "(",
    "]": "["
  }

  for i in bracks:
    if i in list(brack_hash.values()):
      stack.append(i)
    elif i in list(brack_hash.keys()) and len(stack) > 0:
      if stack[-1] == brack_hash[i]:
        stack.pop()
      else:
        print("invalid syntax")
        sys.exit()


  if len(stack) > 0:
    print("invalid syntax")
  else:
    print("valid syntax")

validate_brackets("{{}()()}}}([][]]")